<?php
/**
 * Chipsofic functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Chipsofic
 */

if ( ! function_exists( 'chipsofic_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function chipsofic_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Chipsofic, use a find and replace
		 * to change 'chipsofic' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'chipsofic', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'chipsofic' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'chipsofic_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 78,
			'width'       => 263,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'chipsofic_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function chipsofic_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'chipsofic_content_width', 1140 );
}
add_action( 'after_setup_theme', 'chipsofic_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function chipsofic_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'chipsofic' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here to appear in your sidebar.', 'chipsofic' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'chipsofic' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'chipsofic' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	//Register the siteorigin custom widgets
	if ( defined( 'SITEORIGIN_PANELS_VERSION' ) ) {
		register_widget( 'Chipsoficskill_Widget' );		
		register_widget( 'ChipsoficSectionTitle' );
		register_widget( 'Chipsoficproductblo_Widget' );
	}

}
add_action( 'widgets_init', 'chipsofic_widgets_init' );

/**
 * Load the siteorigin custom widgets.
 */
if ( defined( 'SITEORIGIN_PANELS_VERSION' ) ) {
	require get_template_directory() . "/inc/widgets/widget-skill.php";
	require get_template_directory() . "/inc/widgets/widget-section-title.php";
	require get_template_directory() . "/inc/widgets/widget-product-block.php";
}

/**
 * Register custom fonts.
 */
function chipsofic_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Open Sans, Raleway, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$open_sans = _x( 'on', 'Open Sans font: on or off', 'chipsofic' );
	$raleway = _x( 'on', 'Raleway font: on or off', 'chipsofic' );

	$font_families = array();

	if ( 'off' !== $open_sans ) {
		$font_families[] = 'Open Sans:400,700';		
	}

	if ( 'off' !== $raleway ) {
		$font_families[] = 'Raleway:200,400,700,800';		
	}

	$query_args = array(
		'family' => urlencode( implode( '|', $font_families ) )
	);

	$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );

	return esc_url_raw( $fonts_url );
}

/**
 * Enqueue scripts and styles.
 */
function chipsofic_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'chipsofic-fonts', chipsofic_fonts_url(), array(), null );

	//Bootstrap CSS
	wp_enqueue_style( 'bootstrap', get_theme_file_uri( '/assets/css/bootstrap.min.css' ), array(), '1.0' );

	wp_enqueue_style( 'font-awesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ), array(), '1.0' );

	wp_enqueue_style( 'animate', get_theme_file_uri( '/assets/css/animate.min.css' ), array(), '1.0' );
	
	wp_enqueue_style( 'owl-carousel', get_theme_file_uri( '/assets/css/owl.carousel.css' ), array(), '1.0' );

	wp_enqueue_style( 'magnific-popup', get_theme_file_uri( '/assets/css/magnific-popup.css' ), array(), '1.0' );

	wp_enqueue_style( 'slicknav.min', get_theme_file_uri( '/assets/css/slicknav.min.css' ), array(), '1.0' );

	wp_enqueue_style( 'animated-header', get_theme_file_uri( '/assets/css/animated-header.css' ), array(), '1.0' );

	wp_enqueue_style( 'chipsofic-main-style', get_theme_file_uri( '/assets/style.css' ), array(), '1.0' );

	wp_enqueue_style( 'chipsofic-responsive', get_theme_file_uri( '/assets/css/responsive.css' ), array(), '1.0' );

	wp_enqueue_style( 'chipsofic-stylesheet', get_stylesheet_uri() );

	//Bootstrap JS
	wp_enqueue_script( 'bootstrap', get_theme_file_uri( '/assets/js/bootstrap.min.js' ), array( 'jquery' ), '1.0', true );
	
	wp_enqueue_script( 'magnific-popup', get_theme_file_uri( '/assets/js/jquery.magnific-popup.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'owl-carousel', get_theme_file_uri( '/assets/js/owl.carousel.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'slicknav', get_theme_file_uri( '/assets/js/jquery.slicknav.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'smooth-scroll', get_theme_file_uri( '/assets/js/smooth-scroll.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'wow', get_theme_file_uri( '/assets/js/wow-1.3.0.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'modal', get_theme_file_uri( '/assets/js/modal.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'waypoints', get_theme_file_uri( '/assets/js/waypoints.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'counterup', get_theme_file_uri( '/assets/js/jquery.counterup.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'classie', get_theme_file_uri( '/assets/js/classie.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'cbpAnimatedHeader', get_theme_file_uri( '/assets/js/cbpAnimatedHeader.min.js' ), array(), '1.0', true );
	
	wp_enqueue_script( 'chipsofic-scripts', get_theme_file_uri( '/assets/js/active.js' ), array(), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'chipsofic_scripts' );

/**
 * Register admin JS scripts
 */
if ( ! function_exists( 'chipsofic_admin_enqueue_scripts' ) ) {
	function chipsofic_admin_enqueue_scripts( $hook ) {
		$allowed_hooks = array( 'widgets.php', 'post.php' );

		// enqueue admin utils js, conditionally
		// https://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
		if ( in_array( $hook, $allowed_hooks ) ) {
			wp_enqueue_script( 'chipsofic-admin-utils', get_template_directory_uri() . '/assets/js/admin.js', array( 'jquery' ) );			

			// css for admin
			wp_enqueue_style( 'chipsofic-admin', get_template_directory_uri() . '/assets/css/admin.css' );
		}
	}
	add_action( 'admin_enqueue_scripts', 'chipsofic_admin_enqueue_scripts' );
}


/**
 * Implement the Custom Header feature.
 */
require_once get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require_once get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require_once get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require_once get_template_directory() . '/inc/customizer.php';

/**
 * SiteOrigin Page Builder Extra
 */
require_once get_template_directory() . '/inc/so-page-builder.php';


/**
 * TGM Plugin Activation
 */
require_once get_template_directory() . '/inc/TGM-plugin-activation/tgm-plugin-activation.php';

/**
 * Post Types
 */
require_once get_template_directory() . '/inc/chipsofic-post-types.php';


