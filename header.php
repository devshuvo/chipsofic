<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chipsofic
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'chipsofic' ); ?></a>
    <!-- Main Header Area Start -->
    <div class="header-area" style="background-image: url( <?php header_image(); ?> );">
       <div class="cbp-af-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="logo">
                            <a href="#top">
                                <h1><?php the_custom_logo(); ?></h1>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="main-menu">                                                       
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'navigation',
							) );
							?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Mobile Menu Area Start -->
            <div class="container"><div class="responsive-menu-wrap"></div></div>
        <!-- Mobile Menu Area End -->
        
        <div class="container">
            <div class="row">
                <div class="top-area text-center">
	            <?php			
				if ( is_front_page() && is_home() ) :

					$chipsofic_description = get_bloginfo( 'description', 'display' );
					?>

					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>

					<?php 

					if ( $chipsofic_description || is_customize_preview() ) :

						?>
						<p class="site-description"><?php printf( ' %s ', $chipsofic_description ); /* WPCS: xss ok. */ ?></p>
					<?php endif; ?>

				<?php else :
					?>
					<h1 class="site-title"><?php the_title(); ?></h1>
					<p><?php the_field( 'subtitle' ) ?></p>
					<?php
				endif; ?>
                </div>
            </div>
        </div>

    </div>
    <!-- Main Header Area End -->

	<div id="content" class="content-area clearfix">
		<div class="container">
			<div class="row">
