<?php 
// Register Custom Post Type Gallery
// Post Type Key: gallery
function create_gallery_cpt() {

	$labels = array(
		'name' => __( 'Galleries ', 'Post Type General Name', 'chipsofic' ),
		'singular_name' => __( 'Gallery', 'Post Type Singular Name', 'chipsofic' ),
		'menu_name' => __( 'Galleries ', 'chipsofic' ),
		'name_admin_bar' => __( 'Gallery', 'chipsofic' ),
		'archives' => __( 'Gallery Archives', 'chipsofic' ),
		'attributes' => __( 'Gallery Attributes', 'chipsofic' ),
		'parent_item_colon' => __( 'Parent Gallery:', 'chipsofic' ),
		'all_items' => __( 'All Galleries ', 'chipsofic' ),
		'add_new_item' => __( 'Add New Gallery', 'chipsofic' ),
		'add_new' => __( 'Add New', 'chipsofic' ),
		'new_item' => __( 'New Gallery', 'chipsofic' ),
		'edit_item' => __( 'Edit Gallery', 'chipsofic' ),
		'update_item' => __( 'Update Gallery', 'chipsofic' ),
		'view_item' => __( 'View Gallery', 'chipsofic' ),
		'view_items' => __( 'View Galleries ', 'chipsofic' ),
		'search_items' => __( 'Search Gallery', 'chipsofic' ),
		'not_found' => __( 'Not found', 'chipsofic' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'chipsofic' ),
		'featured_image' => __( 'Featured Image', 'chipsofic' ),
		'set_featured_image' => __( 'Set featured image', 'chipsofic' ),
		'remove_featured_image' => __( 'Remove featured image', 'chipsofic' ),
		'use_featured_image' => __( 'Use as featured image', 'chipsofic' ),
		'insert_into_item' => __( 'Insert into Gallery', 'chipsofic' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Gallery', 'chipsofic' ),
		'items_list' => __( 'Galleries  list', 'chipsofic' ),
		'items_list_navigation' => __( 'Galleries  list navigation', 'chipsofic' ),
		'filter_items_list' => __( 'Filter Galleries  list', 'chipsofic' ),
	);
	$args = array(
		'label' => __( 'Gallery', 'chipsofic' ),
		'description' => __( 'Product gallery post type', 'chipsofic' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-gallery',
		'supports' => array('title', 'thumbnail', 'page-attributes', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'gallery', $args );

}
add_action( 'init', 'create_gallery_cpt', 0 );

// Register Custom Post Type Testimonial
// Post Type Key: testimonial
function create_testimonial_cpt() {

	$labels = array(
		'name' => __( 'Testimonials', 'Post Type General Name', 'chipsofic' ),
		'singular_name' => __( 'Testimonial', 'Post Type Singular Name', 'chipsofic' ),
		'menu_name' => __( 'Testimonials', 'chipsofic' ),
		'name_admin_bar' => __( 'Testimonial', 'chipsofic' ),
		'archives' => __( 'Testimonial Archives', 'chipsofic' ),
		'attributes' => __( 'Testimonial Attributes', 'chipsofic' ),
		'parent_item_colon' => __( 'Parent Testimonial:', 'chipsofic' ),
		'all_items' => __( 'All Testimonials', 'chipsofic' ),
		'add_new_item' => __( 'Add New Testimonial', 'chipsofic' ),
		'add_new' => __( 'Add New', 'chipsofic' ),
		'new_item' => __( 'New Testimonial', 'chipsofic' ),
		'edit_item' => __( 'Edit Testimonial', 'chipsofic' ),
		'update_item' => __( 'Update Testimonial', 'chipsofic' ),
		'view_item' => __( 'View Testimonial', 'chipsofic' ),
		'view_items' => __( 'View Testimonials', 'chipsofic' ),
		'search_items' => __( 'Search Testimonial', 'chipsofic' ),
		'not_found' => __( 'Not found', 'chipsofic' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'chipsofic' ),
		'featured_image' => __( 'Featured Image', 'chipsofic' ),
		'set_featured_image' => __( 'Set featured image', 'chipsofic' ),
		'remove_featured_image' => __( 'Remove featured image', 'chipsofic' ),
		'use_featured_image' => __( 'Use as featured image', 'chipsofic' ),
		'insert_into_item' => __( 'Insert into Testimonial', 'chipsofic' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'chipsofic' ),
		'items_list' => __( 'Testimonials list', 'chipsofic' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'chipsofic' ),
		'filter_items_list' => __( 'Filter Testimonials list', 'chipsofic' ),
	);
	$args = array(
		'label' => __( 'Testimonial', 'chipsofic' ),
		'description' => __( 'Customer testimonials post type', 'chipsofic' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-gallery',
		'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'testimonial', $args );

}
add_action( 'init', 'create_testimonial_cpt', 0 );