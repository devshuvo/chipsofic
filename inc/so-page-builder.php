<?php

// Defaults 
add_theme_support( 'siteorigin-panels', array( 
	'margin-bottom' => 0,
) );


// Add a tab for the theme widgets in the page builder
function chipsofic_theme_widgets_tab($tabs){
	$tabs[] = array(
		'title' => __('Chipsofic Theme Widgets', 'chipsofic'),
		'filter' => array(
			'groups' => array('chipsofic-theme')
		)
	);
	return $tabs;
}
add_filter('siteorigin_panels_widget_dialog_tabs', 'chipsofic_theme_widgets_tab', 20);


// Theme widgets 
function chipsofic_theme_widgets($widgets) {
	$theme_widgets = array(
		'ChipsoficSectionTitle',
		'Chipsoficproductblo_Widget',
		'Chipsoficskill_Widget',
	);
	foreach($theme_widgets as $theme_widget) {
		if( isset( $widgets[$theme_widget] ) ) {
			$widgets[$theme_widget]['groups'] = array('chipsofic-theme');
			$widgets[$theme_widget]['icon'] = 'dashicons dashicons-schedule';
		}
	}
	return $widgets;
}
add_filter('siteorigin_panels_widgets', 'chipsofic_theme_widgets');

// Replace default row options
function chipsofic_row_styles( $fields ) {

	$fields['top_margin'] = array(
		'name'        => __( 'Top Margin', 'chipsofic' ),
		'type'        => 'measurement',
		'group'       => 'layout',
		'description' => __( 'Space above the row, Default is 0px.', 'chipsofic' ),
		'priority'    => 5,
	);

	unset( $fields['collapse_behaviour'] );
	unset( $fields['collapse_order'] );
	unset( $fields['border_color'] );


	return $fields;
}
add_filter('siteorigin_panels_row_style_fields', 'chipsofic_row_styles');

// Filter for the styles
function chipsofic_row_styles_output($attr, $style) {
	//$attr['style'] = '';

	if(!empty($style['top_margin'])) {
		$attr['style'] .= 'margin-top: ' . esc_attr($style['top_margin']) . '; ';
	}

	if(empty($attr['style'])) unset($attr['style']);
	return $attr;
}
add_filter('siteorigin_panels_row_style_attributes', 'chipsofic_row_styles_output', 10, 2);

