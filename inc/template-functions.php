<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Chipsofic
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function chipsofic_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'chipsofic_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function chipsofic_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'chipsofic_pingback_header' );

/**
 * Filter the excerpt "read more" string.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function chipsofic_excerpt_more( $more ) {
    return '';
}
add_filter( 'excerpt_more', 'chipsofic_excerpt_more' );

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function chipsofic_custom_excerpt_length( $length ) {
    return 14;
}
add_filter( 'excerpt_length', 'chipsofic_custom_excerpt_length', 999 );

/**
 *	Chipsofic dynamic css
 *
 * 	@return strings of css on wp_head
 */

function chipsofic_custom_styles() {
	//Dynamic background image for post thumbnail
	global $post;	
	$posts_per_page = get_option( 'posts_per_page' );
	$args = array( 'posts_per_page' => $posts_per_page, 'post_type'=> 'post' );
	$get_posts = get_posts( $args );

	$dynamic_css = '';

	foreach ( $get_posts as $post ) : setup_postdata( $post );
		$dynamic_css .= '
			.blog-bg-'.get_the_ID().' {
    			background-image: url('.get_the_post_thumbnail_url().');
			}
		';
	endforeach; 
	wp_reset_postdata();

	//Header offset fix on Admin mode
	if ( is_admin_bar_showing() ) {
		$dynamic_css .= '
			.header-area .cbp-af-header {
    			top:32px;
			}
			@media only screen and (max-width: 767px)
				.header-area .cbp-af-header {
	    			top:0;
				}
			}
		';
	}
	
	//Output all the styles
	wp_add_inline_style( 'chipsofic-stylesheet', $dynamic_css );
}
add_action( 'wp_enqueue_scripts', 'chipsofic_custom_styles' );

