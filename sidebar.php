<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Chipsofic
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<div class="chipsofic-sidebar widget-area col-sm-4">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div><!-- #secondary -->
