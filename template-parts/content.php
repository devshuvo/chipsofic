<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Chipsofic
 */

?>
<?php
if ( is_single() ) : 
	?>
	<div class="col-sm-12">
		<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
		<div class="post-meta">
			<ul>
				<li><?php chipsofic_posted_on(); ?></li>
				<li><?php chipsofic_tags_list(); ?></li>
			</ul>
		</div>
		<?php the_content(); ?>
	</div>
	<?php
else: 
	?>
	<div class="col-sm-6">
		<a href="<?php the_permalink(); ?>" id="post-<?php the_ID(); ?>" <?php post_class( 'single-blog-item' ); ?>>
			<div class="blog-preview blog-bg-<?php the_ID(); ?>"></div>
			<?php the_title( '<h3 class="entry-title">', '</h3>' ); ?>
			<div class="post-meta">
				<ul>
					<li><?php chipsofic_posted_on(); ?></li>
					<li><?php chipsofic_tags_list(); ?></li>
				</ul>
			</div>
			<?php the_excerpt(); ?>
		</a><!-- #post-<?php the_ID(); ?> -->
	</div>
	<?php
endif;